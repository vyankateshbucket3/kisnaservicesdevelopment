package com.ecommerce.kisna.configuration;
import javax.servlet.Filter;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.ecommerce.kisna.filter.CORSFilter;
/**
 * @author Vyankatesh
 *
 */
public class SpringAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { SpringAppConfiguration.class };
	}

	
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
	 @Override
    protected Filter[] getServletFilters() {
        Filter [] filters = {new CORSFilter()};
       return filters;
  }
}
