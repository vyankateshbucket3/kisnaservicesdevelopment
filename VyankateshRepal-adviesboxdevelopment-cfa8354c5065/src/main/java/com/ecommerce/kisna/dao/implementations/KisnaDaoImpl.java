package com.ecommerce.kisna.dao.implementations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ecommerce.kisna.dao.interfaces.IKisnaDao;
import com.ecommerce.kisna.dto.ConnectionTestDto;
import com.ecommerce.kisna.dto.ProductListDto;
import com.ecommerce.kisna.dto.UserDto;
import com.ecommerce.kisna.exceptions.SpringAppRuntimeException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Vyankatesh
 *
 */
@SuppressWarnings({ "unused" })
@Repository("kisnaDao")
public class KisnaDaoImpl implements IKisnaDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(KisnaDaoImpl.class);
	public static String dateFormat = "dd-MM-yyyy hh:mm:ss";
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
	String jdbcSqlserver = "jdbc:sqlserver://";
	String user = ";user=";
	String password = ";password=";
	String databaseName = ";database=";
	BufferedWriter output = null;

	@Override
	public String connectionTest(ConnectionTestDto connectionTestDto) throws SQLException {
		String output = "";
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(connectionTestDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			if (conn != null) {
				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
				System.out.println("Driver name: " + dm.getDriverName());
				System.out.println("Driver version: " + dm.getDriverVersion());
				System.out.println("Product name: " + dm.getDatabaseProductName());
				System.out.println("Product version: " + dm.getDatabaseProductVersion());
				output = dm.getDriverName().toString();
			}
			/*
			 * String SPsql = "EXEC sp_searchcontact ?,?,?"; PreparedStatement
			 * ps = conn.prepareStatement(SPsql); ps.setEscapeProcessing(true);
			 * ps.setString(1, connectionTestDto.getLastName().toString());
			 * ps.setString(2,
			 * connectionTestDto.getPrimaryEmailAddress().toString());
			 * ps.setString(3,
			 * connectionTestDto.getPrimaryAddressPostalCode().toString());
			 * ResultSet rs = ps.executeQuery(); if (rs.next()) { output =
			 * rs.getString(1); System.out.println(output); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.toString());
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return output;
	}

	@Override
	public JSONObject getMenuSubmenu(ConnectionTestDto connectionTestDto) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(connectionTestDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			/// String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData
			/// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			String SPsql = "{call Proc_GetMenuSubMenu}";
			// PreparedStatement ps = conn.prepareStatement(SPsql);
			List<Map<String, Object>> resultList = executeQueryGetMenuData(sta, SPsql);
			// ResultSet rs = ps.executeQuery();
			jsonOutput.put("menuSubmenu", resultList);
		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	private void closeConnection(Connection conn) throws SpringAppRuntimeException {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
				throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
			}
		}
	}

	public static String getErrorContainingClassAndMethod() {
		final StackTraceElement e = Thread.currentThread().getStackTrace()[2];
		final String s = e.getClassName();
		String errorInMethod = s.substring(s.lastIndexOf('.') + 1, s.length()) + "." + e.getMethodName();
		return "Error in " + errorInMethod + " : ";
	}

	private List<Map<String, Object>> executeQueryGetAllData(Statement sta, String Sql) throws SQLException {
		ResultSet m_ResultSet = sta.executeQuery(Sql);
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		Map<String, Object> row = null;
		ResultSetMetaData metaData = m_ResultSet.getMetaData();
		Integer columnCount = metaData.getColumnCount();
		while (m_ResultSet.next()) {
			row = new HashMap<String, Object>();
			for (int i = 1; i <= columnCount; i++) {
				if (m_ResultSet.getObject(i) != null) {
					row.put(metaData.getColumnName(i), m_ResultSet.getObject(i).toString());
				} else {
					row.put(metaData.getColumnName(i), null);
				}
			}
			resultList.add(row);
		}
		return resultList;
	}

	private List<Map<String, Object>> executeQueryGetMenuData(Statement sta, String Sql) throws SQLException {
		ResultSet m_ResultSet = sta.executeQuery(Sql);
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		Map<String, Object> menu = null;
		Map<String, Object> submenu = null;
		List<Map<String, Object>> submenuList = new ArrayList<Map<String, Object>>();
		ResultSetMetaData metaData = m_ResultSet.getMetaData();
		Integer columnCount = metaData.getColumnCount();
		int i = 0;
		while (m_ResultSet.next()) {
			int tempcatId = m_ResultSet.getInt("CategoryId");

			if (i != m_ResultSet.getInt("CategoryId")) {
				menu = new HashMap<String, Object>();
				menu.put("CategoryId", m_ResultSet.getInt("CategoryId"));
				menu.put("CategoryName", m_ResultSet.getString("CategoryName"));
				menu.put("CategoryCode", m_ResultSet.getString("CategoryCode"));

				submenuList = new ArrayList<Map<String, Object>>();

				menu.put("SubMenu", submenuList);
				resultList.add(menu);

				i = m_ResultSet.getInt("CategoryId");
			}
			submenu = new HashMap<String, Object>();
			submenu.put("SubCategoryId", m_ResultSet.getInt("SubCategoryId"));
			submenu.put("SubCategoryName", m_ResultSet.getString("SubCategoryName"));
			submenu.put("SubCategoryCode", m_ResultSet.getString("SubCategoryCode"));
			submenuList.add(submenu);

		}

		return resultList;
	}

	private Connection createConnection(org.json.JSONObject jsonInput)
			throws ClassNotFoundException, SQLException, JSONException {
		Connection conn;
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		conn = DriverManager.getConnection(
				jdbcSqlserver + jsonInput.getString("serverName") + user + jsonInput.getString("userName") + password
						+ jsonInput.getString("password") + databaseName + jsonInput.getString("databaseName"));
		return conn;
	}

	@Override
	public JSONObject getProductList(ProductListDto productListDto) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(productListDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			/// String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData
			/// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			String SPsql = "{call Proc_GetProductsList(?,?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);
			if (productListDto.getCategoryId() == 0) {
				ps.setNull(1, java.sql.Types.INTEGER);
			} else {
				ps.setObject(1, productListDto.getCategoryId());
			}

			if (productListDto.getSubCategoryId() == 0) {
				ps.setNull(2, java.sql.Types.INTEGER);
			} else {
				ps.setObject(2, productListDto.getSubCategoryId());
			}

			ResultSet rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedure = new ArrayList<Map<String, Object>>();
			Map<String, Object> rowProcedure = null;
			ResultSetMetaData metaDataProcedure = rs.getMetaData();
			Integer columnCountProcedure = metaDataProcedure.getColumnCount();
			int productListCount = 0;
			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				String mrp = (String) rowProcedure.get("MRP");
				String mrpInteger = mrp.substring(0, mrp.length() - 5);
				rowProcedure.put("MRP", mrpInteger);
				resultListProcedure.add(rowProcedure);
				productListCount = resultListProcedure.size();
			}
			jsonOutput.put("result", resultListProcedure);
			jsonOutput.put("count", productListCount);

		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	@Override
	public JSONObject sendMailWithOTP(JSONObject jsonInput) throws SQLException {
		JSONObject jsonOutput = null;
		jsonOutput = sendMailOTP(jsonInput);
		return jsonOutput;
	}

	private JSONObject sendMailOTP(JSONObject jsonInput) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			// Gson gson = new GsonBuilder().create();
			// String json = gson.toJson(mailDto);
			// org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);

			int length = 4;
			char[] OTP = OTP(length);
			System.out.print("Generated OTP is: ");
			System.out.println(OTP);
			String OTPString = String.valueOf(OTP);
			// send an email
			String messageForMail = "Your OTP for Kisna is: " + OTPString;

			// update admin mail and password here
			final String username = "testmails8087@gmail.com";
			final String password = "testmails@123";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			try {

				Message message = new MimeMessage(session);
				// message.setFrom(new InternetAddress("Kisna"));

				// update recipient mail id here.
				String mailTo = (String) jsonInput.get("emailTo");
				// Address address = new InternetAddress("shop@kisna.com");
				// message.setFrom(address);
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
				message.setSubject("Kisna OTP");
				message.setText(messageForMail);

				Transport.send(message);

				System.out.println("OTP sent to mail");
				// check the time when mail is sent
				jsonOutput.put("OTP", OTPString);

				LocalDateTime timeWhenMailSent = LocalDateTime.now();
				jsonOutput.put("timeWhenMailSent", timeWhenMailSent.toString());
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}

		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	static char[] OTP(int len) {
		System.out.println("Generating OTP using random() : ");
		// Using numeric values
		String numbers = "0123456789";
		// Using random method
		Random rndm_method = new Random();
		char[] otp = new char[len];
		for (int i = 0; i < len; i++) {
			// Use of charAt() method : to get character value
			// Use of nextInt() as it is scanning the value as int
			otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return otp;
	}

	@Override
	public JSONObject getProductDetails(ProductListDto productListDto) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(productListDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			String SPsql = "{call Proc_GetProductsDetails(?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);
			if (productListDto.getProductId() == 0) {
				ps.setNull(1, java.sql.Types.INTEGER);
			} else {
				ps.setObject(1, productListDto.getProductId());
			}

			ResultSet rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedure = new ArrayList<Map<String, Object>>();
			Map<String, Object> rowProcedure = null;
			ResultSetMetaData metaDataProcedure = rs.getMetaData();
			Integer columnCountProcedure = metaDataProcedure.getColumnCount();

			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				resultListProcedure.add(rowProcedure);
			}
			jsonOutput.put("productDetails", resultListProcedure);

			// closing statement and connection
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}

			// creating a new connection
			conn = createConnection(jsonInput);
			sta = conn.createStatement();

			// calling second procedure

			SPsql = "{call Proc_GetProductsDetails_Images(?)}";
			ps = conn.prepareStatement(SPsql);
			if (productListDto.getProductId() == 0) {
				ps.setNull(1, java.sql.Types.INTEGER);
			} else {
				ps.setObject(1, productListDto.getProductId());
			}

			rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedureImages = new ArrayList<Map<String, Object>>();
			rowProcedure = null;
			metaDataProcedure = rs.getMetaData();
			columnCountProcedure = metaDataProcedure.getColumnCount();

			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				resultListProcedureImages.add(rowProcedure);
			}
			jsonOutput.put("productImageDetails", resultListProcedureImages);

		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	@Override
	public JSONObject registerUser(UserDto userDto) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(userDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			/// String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData
			/// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			String SPsql = "{call Proc_UserRegistration(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);

			ps.setObject(1, userDto.getFirstName());
			ps.setObject(2, userDto.getLastName());

			ps.setObject(3, userDto.getEmail());
			ps.setObject(4, userDto.getPhone());

			ps.setObject(5, userDto.getGender());
			ps.setObject(6, userDto.getVoiceMail());

			String encryptedPassword = encode(userDto.getPasswordn().toString());

			ps.setObject(7, encryptedPassword);
			ps.setObject(8, userDto.getIsGuest());
			ps.setObject(9, userDto.getAddressStreet1());

			ps.setObject(10, userDto.getAddressStreet2());
			ps.setObject(11, userDto.getCity());

			ps.setObject(12, userDto.getRegion());
			ps.setObject(13, userDto.getPostalCode());

			ps.setObject(14, userDto.getState());
			ps.setObject(15, userDto.getCountry());

			ResultSet rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedure = new ArrayList<Map<String, Object>>();
			Map<String, Object> rowProcedure = null;
			ResultSetMetaData metaDataProcedure = rs.getMetaData();
			Integer columnCountProcedure = metaDataProcedure.getColumnCount();
			int productListCount = 0;
			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				resultListProcedure.add(rowProcedure);
			}
			jsonOutput.put("result", resultListProcedure);

		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	@Override
	public JSONObject login(UserDto userDto) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(userDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			/// String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData
			/// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			String SPsql = "{call Proc_UserLogin(?,?,?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);

			ps.setObject(1, userDto.getEmail());
			ps.setObject(2, userDto.getPhone());

			String encryptedPassword = encode(userDto.getPasswordn().toString());
			ps.setObject(3, encryptedPassword);

			ResultSet rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedure = new ArrayList<Map<String, Object>>();
			Map<String, Object> rowProcedure = null;
			ResultSetMetaData metaDataProcedure = rs.getMetaData();
			Integer columnCountProcedure = metaDataProcedure.getColumnCount();
			int productListCount = 0;
			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				resultListProcedure.add(rowProcedure);
			}
			jsonOutput.put("result", resultListProcedure);

		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	@Override
	public JSONObject sendMessageOTP(JSONObject jsonObject) {
		JSONObject jsonOutput = null;
		jsonOutput = sendMessageOTPTextLocal(jsonObject);
		return jsonOutput;
	}

	private JSONObject sendMessageOTPTextLocal(JSONObject jsonObject) {
		JSONObject jsonOutput = null;
		int length = 4;
		try {
			System.out.println("Generating OTP using random() : ");
			// Using numeric values
			String numbersForOtp = "0123456789";
			// Using random method
			Random rndm_method = new Random();
			char[] otp = new char[length];
			for (int i = 0; i < length; i++) {
				// Use of charAt() method : to get character value
				// Use of nextInt() as it is scanning the value as int
				otp[i] = numbersForOtp.charAt(rndm_method.nextInt(numbersForOtp.length()));
			}

			// char[] OTP = OTP(length);
			System.out.print("Generated OTP is: ");
			System.out.println(otp);
			String OTPString = String.valueOf(otp);
			// send an email
			String messageForMail = "Your OTP for Kisna is: " + OTPString;

			// Construct data
			// String apiKey = "apikey=" +
			// "ixXnmObKYTk-CAsTx4WAwQmTYlOhKNFOlyni9Fd9ax";
			String apiKey = "apikey=" + jsonObject.get("apiKey");
			String message = "&message=" + "Your OTP for Kisna is: " + OTPString;
			String sender = "&sender=" + "TXTLCL";
			// String numbers = "&numbers=" + "918087629345";
			String numbers = "&numbers=" + jsonObject.get("mobileNumber");
			/*
			 * String userName = "&username=" + jsonObject.get("userName");
			 * String hash = "&hash=" + jsonObject.get("hash"); String password
			 * = "&password=" + jsonObject.get("password");
			 */
			String testMessage = "&test=" + jsonObject.get("test");

			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
			String data = apiKey + numbers + message + sender + testMessage;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			System.out.println(stringBuffer.toString());
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(stringBuffer.toString());
			jsonOutput.put("OTP", OTPString);
		} catch (Exception e) {
			System.out.println("Error SMS " + e);
			jsonOutput.put("error", e.toString());
		}
		return jsonOutput;
	}

	@Override
	public JSONObject sendOTP(JSONObject jsonObject) throws SQLException {
		// TODO Auto-generated method stub
		String OTPtype = jsonObject.get("sendTo").toString();
		JSONObject jsonOutput = null;
		if (OTPtype.toLowerCase().equalsIgnoreCase("mail")) {
			jsonOutput = sendMailOTP(jsonObject);
		} else if (OTPtype.toLowerCase().equalsIgnoreCase("message")) {
			jsonOutput = sendMessageOTPTextLocal(jsonObject);
		}
		return jsonOutput;
	}

	public String encode(String raw) {
		return Base64.getUrlEncoder().withoutPadding().encodeToString(raw.getBytes(StandardCharsets.UTF_8));
	}

	@Override
	public JSONObject addProductToCart(JSONObject jsonObject) {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			// Gson gson = new GsonBuilder().create();
			// String json = gson.toJson(productListDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(jsonObject.toJSONString());
			System.out.println(jsonObject);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			String SPsql = "{call Proc_AddProductTocart(?,?,?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);

			ps.setObject(1, jsonObject.get("customerId"));
			ps.setObject(2, jsonObject.get("productId"));
			ps.setObject(3, jsonObject.get("orderQuantity"));

			ResultSet rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedure = new ArrayList<Map<String, Object>>();
			Map<String, Object> rowProcedure = null;
			ResultSetMetaData metaDataProcedure = rs.getMetaData();
			Integer columnCountProcedure = metaDataProcedure.getColumnCount();

			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				resultListProcedure.add(rowProcedure);
			}
			jsonOutput.put("details", resultListProcedure);

			// closing statement and connection
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			jsonOutput.put("error", e.toString());
		}
		return jsonOutput;
	}

	@Override
	public JSONObject getProductListNewArrival(JSONObject jsonObject) throws SQLException {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			// Gson gson = new GsonBuilder().create();
			// String json = gson.toJson(connectionTestDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(jsonObject.toJSONString());
			System.out.println(jsonInput);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			/// String SPsql = "{call SP_UpdateAdvisoryBoxPersonalData
			/// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			String SPsql = "{call Proc_GetProductsList_NewArrival}";
			// PreparedStatement ps = conn.prepareStatement(SPsql);
			List<Map<String, Object>> resultList = executeQueryGetAllData(sta, SPsql);
			// ResultSet rs = ps.executeQuery();
			jsonOutput.put("newArrivalProductsList", resultList);
		} catch (Exception e) {
			output = e.toString();
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		} finally {
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return jsonOutput;
	}

	@Override
	public JSONObject getUserCartDetails(JSONObject jsonObject) {
		String output = null;
		Connection conn = null;
		Statement sta = null;
		String errorClassAndMethod = getErrorContainingClassAndMethod();
		JSONObject jsonOutput = new JSONObject();
		try {
			// Gson gson = new GsonBuilder().create();
			// String json = gson.toJson(productListDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(jsonObject.toJSONString());
			System.out.println(jsonObject);
			conn = createConnection(jsonInput);
			sta = conn.createStatement();
			String SPsql = "{call Proc_GetUserCartDetails(?)}";
			PreparedStatement ps = conn.prepareStatement(SPsql);

			ps.setObject(1, jsonObject.get("customerId"));

			ResultSet rs = ps.executeQuery();
			List<Map<String, Object>> resultListProcedure = new ArrayList<Map<String, Object>>();
			Map<String, Object> rowProcedure = null;
			ResultSetMetaData metaDataProcedure = rs.getMetaData();
			Integer columnCountProcedure = metaDataProcedure.getColumnCount();

			while (rs.next()) {
				rowProcedure = new HashMap<String, Object>();
				for (int i = 1; i <= columnCountProcedure; i++) {
					if (rs.getObject(i) != null) {
						rowProcedure.put(metaDataProcedure.getColumnName(i), rs.getObject(i).toString());
					} else {
						rowProcedure.put(metaDataProcedure.getColumnName(i), null);
					}
				}
				String mrp = (String) rowProcedure.get("MRP");
				String mrpInteger = mrp.substring(0, mrp.length() - 5);
				rowProcedure.put("MRP", mrpInteger);
				resultListProcedure.add(rowProcedure);
			}
			jsonOutput.put("userCartdetails", resultListProcedure);

			// closing statement and connection
			if (sta != null) {
				conn.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			jsonOutput.put("error", e.toString());
		}
		return jsonOutput;
	}

}