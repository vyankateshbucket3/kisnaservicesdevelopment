package com.ecommerce.kisna.configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.ecommerce.kisna.filter.CORSFilter;

/**
 * @author Vyankatesh
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.ecommerce.kisna")
public class SpringAppConfiguration extends WebMvcConfigurerAdapter {
	/**
	 * Configure ViewResolvers to deliver preferred views.
	 */
	@Bean
	public CORSFilter corsFilter() {
		CORSFilter corsFilter = new CORSFilter();
		return corsFilter;
	}
	
	 @Bean
	    public WebMvcConfigurer corsConfigurer() {
	        return new WebMvcConfigurerAdapter() {
	            @Override
	            public void addCorsMappings(CorsRegistry registry) {
	                registry.addMapping("/**")
	                .allowedOrigins("http://10.235.5.3:8080", 
	                		"http://localhost:4200",
	                		"http://104.130.128.208:8080",
 				        "http://localhost:8080",
 				        "http://127.0.0.1:8080")
	                .allowedMethods("PUT", "POST","DELETE","GET","OPTIONS")
	                .allowedHeaders("Origin","XMLHttpRequest", "X-Requested-With", "Content-Type"," Accept", "Authorization", "api_key")
	        		.exposedHeaders("content-length", "header2")
	    		    .allowCredentials(false).maxAge(3600);
	            }
	        };
	    }
}
