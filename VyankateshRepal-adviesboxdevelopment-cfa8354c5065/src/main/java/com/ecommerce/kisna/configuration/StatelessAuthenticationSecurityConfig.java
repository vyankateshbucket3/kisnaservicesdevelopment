package com.ecommerce.kisna.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.AbstractRequestMatcherRegistry;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.header.HeaderWriterFilter;

import com.ecommerce.kisna.filter.CORSFilter;

/**
 * @author Vyankatesh
 *
 */
@EnableWebSecurity
@Configuration
@Order(2)
public class StatelessAuthenticationSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	CORSFilter corsFilter;
	
	public StatelessAuthenticationSecurityConfig() {
		super(true);
	}

	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");

	}

	private AbstractRequestMatcherRegistry<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.AuthorizedUrl> ignoring() {
		return null;
	}
	

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.formLogin()
        	.and()
	        .exceptionHandling()
	        .and()
			.anonymous()
			.and()
			.servletApi()
			.and()
            .authorizeRequests()
                .antMatchers("/**").permitAll()
                /*
                .antMatchers("/landing").access("hasRole('SchoolAdmin')")
                .antMatchers("/schoolAdmin/**").access("hasRole('SchoolAdmin')")
                .antMatchers("/superUser/**").access("hasRole('SuperUser')")
                .antMatchers("/teacher/**").access("hasRole('Teacher')")
                .antMatchers("/student/**").access("hasRole('Student')")
                .antMatchers("/parrent/**").access("hasRole('Parrent')")
                */
                .antMatchers(HttpMethod.OPTIONS, "/*/**").permitAll()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(corsFilter, HeaderWriterFilter.class)
				//.addFilterBefore(new StatelessLoginFilter("/login", tokenAuthenticationService, userDetailsService, userRolesService, authenticationManager()), UsernamePasswordAuthenticationFilter.class)                 // custom JSON based authentication by POST of {"username":"<name>","password":"<password>"} which sets the token header upon authentication
				//.addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService), UsernamePasswordAuthenticationFilter.class)			// custom Token based authentication based on the header previously given to the client
        		.csrf().disable();
        		/*
        		.csrfTokenRepository(csrfTokenRepository())
        		.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class);
        		*/

   }

	private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}

}



