package com.ecommerce.kisna.services.implementation;

import java.sql.SQLException;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.kisna.dao.interfaces.IKisnaDao;
import com.ecommerce.kisna.dto.ConnectionTestDto;
import com.ecommerce.kisna.dto.MailDto;
import com.ecommerce.kisna.dto.ProductListDto;
import com.ecommerce.kisna.dto.UserDto;
import com.ecommerce.kisna.services.interfaces.IKisna;

@Service("kisnaService")
public class KisnaServiceImpl implements IKisna {

	@Autowired
	IKisnaDao kisnaDao;

	@Override
	public String connectionTest(ConnectionTestDto connectionTestDto) throws SQLException {
		return kisnaDao.connectionTest(connectionTestDto);
	}

	@Override
	public JSONObject getMenuSubmenu(ConnectionTestDto connectionTestDto) throws SQLException {
		return kisnaDao.getMenuSubmenu(connectionTestDto);
	}

	@Override
	public JSONObject getProductList(ProductListDto productListDto) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.getProductList(productListDto);
	}

	@Override
	public JSONObject sendMailWithOTP(JSONObject jsonInput) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.sendMailWithOTP( jsonInput);
	}

	@Override
	public JSONObject getProductDetails(ProductListDto productListDto) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.getProductDetails(productListDto);
	}

	@Override
	public JSONObject registerUser(UserDto userDto) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.registerUser(userDto);
	}

	@Override
	public JSONObject login(UserDto userDto) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.login(userDto);
	}

	@Override
	public JSONObject sendMessageOTP(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		return kisnaDao.sendMessageOTP(jsonObject);
	}

	@Override
	public JSONObject sendOTP(JSONObject jsonObject) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.sendOTP( jsonObject);
	}

	@Override
	public JSONObject addProductToCart(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		return kisnaDao.addProductToCart(jsonObject);
	}

	@Override
	public JSONObject getProductListNewArrival(JSONObject jsonObject) throws SQLException {
		// TODO Auto-generated method stub
		return kisnaDao.getProductListNewArrival(jsonObject);
	}

	@Override
	public JSONObject getUserCartDetails(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		return kisnaDao.getUserCartDetails(jsonObject);
	}

}
