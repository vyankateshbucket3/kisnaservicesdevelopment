package com.ecommerce.kisna.services.interfaces;

import org.json.simple.JSONObject;


/**
 * @author Vyankatesh
 *
 */
public interface ISearchServices {

	public JSONObject listDatasources();

	public JSONObject createDatasource(JSONObject jsonInput);

	public JSONObject createIndex(JSONObject jsonInput);

	public JSONObject createIndexer(JSONObject jsonInput);

	public JSONObject createIndexerFieldMappings(JSONObject jsonInput);

}
