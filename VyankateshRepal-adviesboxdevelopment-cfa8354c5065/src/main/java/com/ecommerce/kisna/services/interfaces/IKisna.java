package com.ecommerce.kisna.services.interfaces;

import java.sql.SQLException;

import org.json.simple.JSONObject;

import com.ecommerce.kisna.dto.ConnectionTestDto;
import com.ecommerce.kisna.dto.MailDto;
import com.ecommerce.kisna.dto.ProductListDto;
import com.ecommerce.kisna.dto.UserDto;

public interface IKisna {

	public String connectionTest(ConnectionTestDto connectionTestDto) throws SQLException;

	public JSONObject getMenuSubmenu(ConnectionTestDto connectionTestDto) throws SQLException;

	public JSONObject getProductList(ProductListDto productListDto) throws SQLException;

	public JSONObject sendMailWithOTP(JSONObject jsonInput) throws SQLException;

	public JSONObject getProductDetails(ProductListDto productListDto) throws SQLException;

	public JSONObject registerUser(UserDto userDto) throws SQLException;

	public JSONObject login(UserDto userDto) throws SQLException;

	public JSONObject sendMessageOTP(JSONObject jsonObject);

	public JSONObject sendOTP(JSONObject jsonObject) throws SQLException;

	public JSONObject addProductToCart(JSONObject jsonObject);

	public JSONObject getProductListNewArrival(JSONObject jsonObject) throws SQLException;

	public JSONObject getUserCartDetails(JSONObject jsonObject);


	
}
