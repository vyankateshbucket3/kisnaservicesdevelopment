package com.ecommerce.kisna.controller;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.activation.FileTypeMap;
import javax.ws.rs.PathParam;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ccavenue.security.AesCryptUtil;
import com.ecommerce.kisna.dto.ConnectionTestDto;
import com.ecommerce.kisna.dto.MailDto;
import com.ecommerce.kisna.dto.ProductListDto;
import com.ecommerce.kisna.dto.UserDto;
import com.ecommerce.kisna.services.interfaces.IKisna;
import com.google.common.net.HttpHeaders;

import io.jsonwebtoken.lang.Arrays;

/**
 * @author Vyankatesh
 * 
 *
 */
@RestController
@RequestMapping("/kisna")
public class KisnaController {
	@Autowired
	IKisna kisnaService;

	@RequestMapping(value = "/connectionTest", method = RequestMethod.POST)
	public ResponseEntity<String> connectionTest(@RequestBody ConnectionTestDto connectionTestDto)
			throws NumberFormatException, Exception {
		String result = kisnaService.connectionTest(connectionTestDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/testAPI", method = RequestMethod.GET)
	public String connectionTest() throws NumberFormatException, Exception {
		/*
		 * String result = kisnaService.connectionTest(); if (result == null) {
		 * return new ResponseEntity<>(HttpStatus.NO_CONTENT); }
		 */
		return "success";
	}

	@RequestMapping(value = "/getMenuSubmenu", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getMenuSubmenu(@RequestBody ConnectionTestDto connectionTestDto)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.getMenuSubmenu(connectionTestDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getProductList", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getProductList(@RequestBody ProductListDto productListDto)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.getProductList(productListDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/sendMailWithOTP", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> sendMailWithOTP(@RequestBody JSONObject jsonInput)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.sendMailWithOTP(jsonInput);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/postImage", method = RequestMethod.POST)
	public ResponseEntity<byte[]> getImage(@RequestBody JSONObject jsonObject) throws IOException {
		String filePath = jsonObject.get("filePath").toString();
		File img = new File(filePath);
		System.out.println();
		return ResponseEntity.ok()
				.contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img)))
				.body(Files.readAllBytes(img.toPath()));
	}

	@RequestMapping(value = "/getImage/{filePath}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImage(@PathParam("filePath") String filePath) throws IOException {
		File img = new File(filePath);
		System.out.println();
		return ResponseEntity.ok()
				.contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img)))
				.body(Files.readAllBytes(img.toPath()));
	}

	@RequestMapping(value = "/getProductDetails", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getProductDetails(@RequestBody ProductListDto productListDto)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.getProductDetails(productListDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> registerUser(@RequestBody UserDto userDto)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.registerUser(userDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> login(@RequestBody UserDto userDto) throws NumberFormatException, Exception {
		JSONObject result = kisnaService.login(userDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/sendMessageOTP", method = RequestMethod.POST)
	// public ResponseEntity<String> checkServicesStatus()
	public ResponseEntity<JSONObject> sendMessageOTP(@RequestBody JSONObject jsonObject)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.sendMessageOTP(jsonObject);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// ccavenue api
	@RequestMapping(value = "/payment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> pay(@RequestBody HashMap<String, String> paramMap) {
		try {
			paramMap.get("ammount");
			paramMap.get("order_id");
			String workingKey = "3DEBA01B7E98178B0D1572D15BC4304E"; // 32 Bit
																	// Alphanumeric
																	// Working
																	// Key
																	// should be
																	// entered
																	// here so
																	// that data
																	// can be
																	// decrypted.
			String accessCode = "AVNK66DG04AW95KNWA";
			String url = "https://secure.ccavenue.com/transaction.do?command=initiateTransaction";
			String ccaRequest = "?merchant_id=103622&access_code=AVNK66DG04AW95KNWA&working_key=3DEBA01B7E98178B0D1572D15BC4304E&request_url='test2.kisna.com'&currency=INR&response_url='test2.kisna.com/payment_response'&ammount="
					+ paramMap.get("ammount") + "&order_id=" + paramMap.get("order_id");
			;
			AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
			String encRequest = aesUtil.encrypt(ccaRequest);
			System.out.println(encRequest);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> entity = new HttpEntity<String>("parameters");
			ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			return result;
		} catch (Exception e) {
			return new ResponseEntity<String>("Failed to make payment", HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/sendOTP", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> sendOTP(@RequestBody JSONObject jsonObject)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.sendOTP(jsonObject);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addProductToCart", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> addProductToCart(@RequestBody JSONObject jsonObject)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.addProductToCart(jsonObject);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProductListNewArrival", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getProductListNewArrival(@RequestBody JSONObject jsonObject)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.getProductListNewArrival(jsonObject);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getUserCartDetails", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getUsercartDetails(@RequestBody JSONObject jsonObject)
			throws NumberFormatException, Exception {
		JSONObject result = kisnaService.getUserCartDetails(jsonObject);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
