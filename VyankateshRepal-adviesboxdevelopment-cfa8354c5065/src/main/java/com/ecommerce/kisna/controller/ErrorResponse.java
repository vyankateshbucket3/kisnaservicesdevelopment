/*
* License to Spring Server Boot Project
 * To be used for internal zcon projects only 
 */
package com.ecommerce.kisna.controller;

/**
 *
 * @author manisha
 */
public class ErrorResponse {

	private int errorCode;
	private String message;
	
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

    
    
}
