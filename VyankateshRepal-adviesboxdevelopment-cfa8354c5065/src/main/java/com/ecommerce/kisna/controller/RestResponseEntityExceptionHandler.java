/*
 * License to Spring Server Boot Project
 * To be used for internal zcon projects only
 */
package com.ecommerce.kisna.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ecommerce.kisna.exceptions.SpringAppRuntimeException;


/**
 *
 * @author manisha
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    // No handler found exception
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected ResponseEntity handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        LOGGER.error("handleNoHandlerFoundException....", ex);
        ex.printStackTrace();
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("Application runtime exception occurred. [" + ex.getMessage() + "]");
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // handle application exception
    @ExceptionHandler(SpringAppRuntimeException.class)
    protected ResponseEntity<ErrorResponse> handleSpringAppRuntimeException(SpringAppRuntimeException ex) {
        LOGGER.error("SpringAppRuntimeException....", ex.toString());
        ex.printStackTrace();
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("Application runtime exception occurred. [" + ex.getMessage() + "]");
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // handle all other exceptions
    @ExceptionHandler(value = { Exception.class })
    protected ResponseEntity<ErrorResponse> handleException(Exception ex) {
        LOGGER.error("Generic Exception.... ", ex);
        ex.printStackTrace();
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("General exception occurred. [" + ex.toString() + "].");
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
