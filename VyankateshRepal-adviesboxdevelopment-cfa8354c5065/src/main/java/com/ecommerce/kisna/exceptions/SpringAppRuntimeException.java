/*
 * License to Spring Server Boot Project
 * To be used for internal zcon projects only 
 */
package com.ecommerce.kisna.exceptions;

/**
 *
 * @author Vyankatesh
 */
public class SpringAppRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public SpringAppRuntimeException() {
        super();
    }

    public SpringAppRuntimeException(String message, Throwable cause) {
            super(message, cause);
    }

    public SpringAppRuntimeException(String message) {
            super(message);
    }

    public SpringAppRuntimeException(Throwable cause) {
            super(cause);
    }

}