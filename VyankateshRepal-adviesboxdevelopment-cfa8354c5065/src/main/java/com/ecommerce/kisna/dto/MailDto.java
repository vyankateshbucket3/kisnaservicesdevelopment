package com.ecommerce.kisna.dto;

public class MailDto {
	
	private String emailTo;
	/*private String emailFro;
	private String primaryAddressPostalCode;
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;*/

	public String getEmailTo() {
		return emailTo;
	}

	@Override
	public String toString() {
		return "MailDto [emailTo=" + emailTo + "]";
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

}
