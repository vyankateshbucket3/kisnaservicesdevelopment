package com.ecommerce.kisna.dto;

public class ProductListDto {

	private String primaryEmailAddress;
	private String lastName;
	private String primaryAddressPostalCode;
	private String serverName;
	private String userName;
	private String password;
	private String databaseName;

	private int categoryId;
	private int subCategoryId;
	private int productId;

	public String getPrimaryEmailAddress() {
		return primaryEmailAddress;
	}

	public void setPrimaryEmailAddress(String primaryEmailAddress) {
		this.primaryEmailAddress = primaryEmailAddress;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPrimaryAddressPostalCode() {
		return primaryAddressPostalCode;
	}

	public void setPrimaryAddressPostalCode(String primaryAddressPostalCode) {
		this.primaryAddressPostalCode = primaryAddressPostalCode;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "ProductListDto [primaryEmailAddress=" + primaryEmailAddress + ", lastName=" + lastName
				+ ", primaryAddressPostalCode=" + primaryAddressPostalCode + ", serverName=" + serverName
				+ ", userName=" + userName + ", password=" + password + ", databaseName=" + databaseName
				+ ", categoryId=" + categoryId + ", subCategoryId=" + subCategoryId + ", productId=" + productId + "]";
	}

	
}
