package com.ecommerce.kisna.dto;


public class UserDto {

	private String serverName;
	private String userName;
	private String password;
	private String databaseName;
	
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String gender;
	private String voiceMail;
	private String passwordn;
	private String addressStreet1;
	private String addressStreet2;
	private String city;
	private String region;
	private String postalCode;
	private String state;
	private String country;
	private String isGuest;
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getVoiceMail() {
		return voiceMail;
	}
	public void setVoiceMail(String voiceMail) {
		this.voiceMail = voiceMail;
	}
	public String getPasswordn() {
		return passwordn;
	}
	public void setPasswordn(String passwordn) {
		this.passwordn = passwordn;
	}
	public String getAddressStreet1() {
		return addressStreet1;
	}
	public void setAddressStreet1(String addressStreet1) {
		this.addressStreet1 = addressStreet1;
	}
	public String getAddressStreet2() {
		return addressStreet2;
	}
	public void setAddressStreet2(String addressStreet2) {
		this.addressStreet2 = addressStreet2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getIsGuest() {
		return isGuest;
	}
	public void setIsGuest(String isGuest) {
		this.isGuest = isGuest;
	}
	@Override
	public String toString() {
		return "UserDto [serverName=" + serverName + ", userName=" + userName + ", password=" + password
				+ ", databaseName=" + databaseName + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + ", phone=" + phone + ", gender=" + gender + ", voiceMail=" + voiceMail + ", passwordn="
				+ passwordn + ", addressStreet1=" + addressStreet1 + ", addressStreet2=" + addressStreet2 + ", city="
				+ city + ", region=" + region + ", postalCode=" + postalCode + ", state=" + state + ", country="
				+ country + ", isGuest=" + isGuest + "]";
	}

	

	

	
}
